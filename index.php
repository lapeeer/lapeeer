<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- owl -->
    <link rel="stylesheet" href="js/owl/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="js/owl/assets/owl.theme.default.min.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Actward Project</title>
  </head>
  <body>
    <section id="wrap-header">
      <div class="container">
          <div class="row">
            <h3>Actward Project</h3>
            <h5></h5>
            <!-- <div class="owl-carousel owl-theme">
                    <div class="item">
                      <h4>1</h4>
                    </div>
                    <div class="item">
                      <h4>2</h4>
                    </div>
                    <div class="item">
                      <h4>3</h4>
                    </div>
                    <div class="item">
                      <h4>4</h4>
                    </div>
                    <div class="item">
                      <h4>5</h4>
                    </div>
                    <div class="item">
                      <h4>6</h4>
                    </div>
                    <div class="item">
                      <h4>7</h4>
                    </div>
                    <div class="item">
                      <h4>8</h4>
                    </div>
                    <div class="item">
                      <h4>9</h4>
                    </div>
                    <div class="item">
                      <h4>10</h4>
                    </div>
                    <div class="item">
                      <h4>11</h4>
                    </div>
                    <div class="item">
                      <h4>12</h4>
                    </div>
                  </div> -->
          </div>
      </div>
    </section>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    -->

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="js/owl/owl.carousel.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:false
                }
            }
          })
        });
    </script>
  </body>
</html>
